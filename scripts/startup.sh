export TWITTER_CONSUMER_API_KEY=$(echo ${ENV_VARS} | jq -r '.TWITTER_CONSUMER_API_KEY')
export TWITTER_CONSUMER_API_KEY_SECRET=$(echo ${ENV_VARS} | jq -r '.TWITTER_CONSUMER_API_KEY_SECRET')
export POSTGRES_USERNAME=$(echo ${ENV_VARS} | jq -r '.POSTGRES_USERNAME')
export POSTGRES_PASSWORD=$(echo ${ENV_VARS} | jq -r '.POSTGRES_PASSWORD')
export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.LOG_LEVEL')
export MIGRATE=$(echo ${ENV_VARS} | jq -r '.MIGRATE')
export AUTH_TOKEN=$(echo ${ENV_VARS} | jq -r '.AUTH_TOKEN')

export TWITTER_TOKENS=$(echo ${TWITTER_TOKENS})

/twitter
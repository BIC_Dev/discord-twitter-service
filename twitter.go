package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/BIC_Dev/discord-twitter-service/controllers"
	"gitlab.com/BIC_Dev/discord-twitter-service/models"
	"gitlab.com/BIC_Dev/discord-twitter-service/routes"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils/db"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
}

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	service := Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	if os.Getenv("MIGRATE") == "TRUE" {
		migrationErrors := service.migrateTables(models.Twitter{})

		if migrationErrors != nil {
			service.Log.Log(migrationErrors, service.Log.LogFatal)
		}
	}

	controller := controllers.Controller{
		Config:    service.Config,
		Log:       service.Log,
		AuthToken: os.Getenv("AUTH_TOKEN"),
	}

	service.Log.Log("PROCESS: Setting up router", service.Log.LogInformation)

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller)

	service.Log.Log("SUCCESS: Set up router", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting MUX listener", service.Log.LogInformation)
	routes.StartListener(router)
}

func (s *Service) getDB() (db.Interface, *utils.ModelError) {
	var DBStruct db.Interface
	var err *utils.ModelError

	switch s.Config.DB.Type {
	case "SQLite3":
		s.Log.Log("Connecting to SQLite3 DB", s.Log.LogInformation)
		SQLite3 := db.SQLite3{}
		err = SQLite3.Connect(s.Config)

		if err != nil {
			s.Log.Log(fmt.Sprintf("%s: %s", err.GetMessage(), err.Error()), s.Log.LogHigh)
		}

		DBStruct = &SQLite3
	case "PostgreSQL":
		s.Log.Log("Connecting to PostgreSQL", s.Log.LogInformation)
		PostgreSQL := db.PostgreSQL{
			Username: os.Getenv("POSTGRES_USERNAME"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
		}

		err = PostgreSQL.Connect(s.Config)

		if err != nil {
			s.Log.Log(fmt.Sprintf("%s: %s", err.GetMessage(), err.Error()), s.Log.LogHigh)
		}

		DBStruct = &PostgreSQL
	default:
		s.Log.Log("Invalid DB type", s.Log.LogFatal)
	}

	if err != nil {
		return nil, err
	}

	s.Log.Log("Succesfully connected to DB", s.Log.LogInformation)

	return DBStruct, nil
}

func (s *Service) migrateTables(tables ...interface{}) []*utils.ModelError {
	s.Log.Log("Migrating tables: twitter, facebook", s.Log.LogInformation)
	var migrationErrors []*utils.ModelError

	dbStruct, dbErr := db.GetDB(s.Config)

	if dbErr != nil {
		s.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), s.Log.LogFatal)
	}

	defer dbStruct.GetDB().Close()

	for _, table := range tables {
		migrationError := dbStruct.Migrate(table)

		if migrationError != nil {
			migrationErrors = append(migrationErrors, migrationError)
		}
	}

	if len(migrationErrors) > 0 {
		return migrationErrors
	}

	return nil
}

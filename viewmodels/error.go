package viewmodels

// ErrorResponse response struct for an error
type ErrorResponse struct {
	StatusCode int    `json:"-"`
	Error      string `json:"error"`
	Message    string `json:"message"`
}

// EmbeddedError struct
type EmbeddedError struct {
	DiscordMessageID string `json:"discord_message_id"`
	Message          string `json:"message"`
	Error            string `json:"error"`
}

// NewErrorResponse creates a new error response
func NewErrorResponse(status int, err string, message string) ErrorResponse {
	return ErrorResponse{
		StatusCode: status,
		Error:      err,
		Message:    message,
	}
}

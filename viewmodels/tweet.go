package viewmodels

import (
	"net/http"

	"github.com/go-playground/validator"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils"
)

// CreateDiscordRequest struct
type CreateDiscordRequest struct {
	GuildID         string `json:"guild_id" validate:"required"`
	GuildName       string `json:"guild_name" validate:"required"`
	ChannelID       string `json:"channel_id" validate:"required"`
	ChannelName     string `json:"channel_name" validate:"required"`
	MessageID       string `json:"message_id" validate:"required"`
	MessageAuthorID string `json:"message_author_id" validate:"required"`
	MessageAuthor   string `json:"message_author" validate:"required"`
}

// CreateTweetRequest request for creating a user
type CreateTweetRequest struct {
	Discord     *CreateDiscordRequest `json:"discord" validate:"required"`
	MessageText string                `json:"message_text"`
	MediaURLs   []string              `json:"media_urls" validate:"required"`
}

// CreateTweetResponse struct
type CreateTweetResponse struct {
	Status        int              `json:"status"`
	Message       string           `json:"message"`
	TwitterIDs    []string         `json:"twitter_ids"`
	TwitterUrls   []string         `json:"twitter_urls"`
	TwitterErrors []*EmbeddedError `json:"twitter_errors"`
	DBErrors      []*EmbeddedError `json:"db_errors"`
}

// DeleteDiscordRequest struct
type DeleteDiscordRequest struct {
	GuildID    string   `json:"guild_id" validate:"required"`
	MessageIDs []string `json:"message_ids" validate:"required"`
}

// DeleteTweetRequest request for creating a user
type DeleteTweetRequest struct {
	Discord *DeleteDiscordRequest `json:"discord" validate:"required"`
}

// DeleteTweetResponse struct
type DeleteTweetResponse struct {
	Status        int              `json:"status"`
	Message       string           `json:"message"`
	DiscordIDs    []string         `json:"discord_ids"`
	TwitterErrors []*EmbeddedError `json:"twitter_errors"`
	DBErrors      []*EmbeddedError `json:"DBErrors"`
}

// Validate validates the create user request params
func (r *CreateTweetRequest) Validate() *utils.ValidationError {
	var validate *validator.Validate
	validate = validator.New()

	if err := validate.Struct(r); err != nil {
		validationError := utils.NewValidationError(err)
		validationError.SetStatus(http.StatusBadRequest)
		validationError.SetMessage("Validation failed on request to create a new tweet")

		return validationError
	}

	return nil
}

// Validate validates the create user request params
func (r *DeleteTweetRequest) Validate() *utils.ValidationError {
	var validate *validator.Validate
	validate = validator.New()

	if err := validate.Struct(r); err != nil {
		validationError := utils.NewValidationError(err)
		validationError.SetStatus(http.StatusBadRequest)
		validationError.SetMessage("Validation failed on request to delete a tweet")

		return validationError
	}

	if err := validate.Struct(r.Discord); err != nil {
		validationError := utils.NewValidationError(err)
		validationError.SetStatus(http.StatusBadRequest)
		validationError.SetMessage("Validation failed on request to delete a tweet")

		return validationError
	}

	return nil
}

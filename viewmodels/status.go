package viewmodels

// GetStatusResponse struct
type GetStatusResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

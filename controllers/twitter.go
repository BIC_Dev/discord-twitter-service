package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/ChimeraCoder/anaconda"
	"gitlab.com/BIC_Dev/discord-twitter-service/models"
	"gitlab.com/BIC_Dev/discord-twitter-service/services"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils/db"
	"gitlab.com/BIC_Dev/discord-twitter-service/viewmodels"
)

// CreateTweet creates a Twitter post
func (c *Controller) CreateTweet(w http.ResponseWriter, r *http.Request) {
	c.Log.Log("PROCESS: CreateTweet route called", c.Log.LogInformation)

	if r.Header.Get("Auth-Token") != c.AuthToken {
		c.Log.Log("BLOCKED: Invalid auth token for CreateTweet request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid auth token", "Invalid auth token for CreateTweet request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, err.Error(), "Could not read body of request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	var request viewmodels.CreateTweetRequest
	json.Unmarshal(reqBody, &request)

	c.Log.Log("PROCESS: CreateTweet request validation", c.Log.LogInformation)

	validationErr := request.Validate()

	if validationErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: CreateTweet request validation failed: %s", validationErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, validationErr.Error(), validationErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	c.Log.Log("SUCCESS: CreateTweet request validation passed", c.Log.LogInformation)

	twitterClient, clientErr := services.GetTwitterClient(request.Discord.GuildID)

	if clientErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to create Twitter client: %s", clientErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, clientErr.Error(), "Unable to create Twitter client")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	twitter := services.Twitter{
		Client: twitterClient,
	}

	dbStruct, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbStruct.GetDB().Close()

	var tweets []*anaconda.Tweet
	var tweetErrs []*viewmodels.EmbeddedError
	var dbErrs []*viewmodels.EmbeddedError

	for _, mediaURL := range request.MediaURLs {
		tp := services.TwitterPost{
			MediaURL:         mediaURL,
			MediaDescription: request.MessageText,
			MediaAuthor:      request.Discord.MessageAuthor,
			GuildName:        request.Discord.GuildName,
		}

		c.Log.Log(fmt.Sprintf("PROCESS: Creating Twitter post for Discord Message ID: %s", request.Discord.MessageID), c.Log.LogInformation)

		aTweet, tweetErr := twitter.CreatePost(&tp)

		if tweetErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Failed to post to Twitter for Message ID (%s): %s", request.Discord.MessageID, tweetErr.Error()), c.Log.LogHigh)
			embErr := viewmodels.EmbeddedError{
				DiscordMessageID: request.Discord.MessageID,
				Message:          "Failed to post to Twitter",
				Error:            tweetErr.Error(),
			}

			tweetErrs = append(tweetErrs, &embErr)
			continue
		}

		c.Log.Log(fmt.Sprintf("SUCCESS: Twitter message successfully posted for Discord Message ID: %s", request.Discord.MessageID), c.Log.LogInformation)

		tweets = append(tweets, aTweet)

		twitterModel := models.Twitter{
			TwitterID:               aTweet.IdStr,
			DiscordMessageID:        request.Discord.MessageID,
			DiscordGuildID:          request.Discord.GuildID,
			DiscordGuildName:        request.Discord.GuildName,
			DiscordChannelID:        request.Discord.ChannelID,
			DiscordChannelName:      request.Discord.ChannelName,
			DiscordAuthorID:         request.Discord.MessageAuthorID,
			DiscordAuthorName:       request.Discord.MessageAuthor,
			DiscordMediaURL:         mediaURL,
			DiscordMediaDescription: request.MessageText,
		}

		c.Log.Log(fmt.Sprintf("PROCESS: Adding Twitter post to DB for Discord Message ID: %s", request.Discord.MessageID), c.Log.LogInformation)

		twitterDBErr := twitterModel.Create(dbStruct)

		if twitterDBErr != nil {
			embErr := viewmodels.EmbeddedError{
				DiscordMessageID: request.Discord.MessageID,
				Message:          twitterDBErr.GetMessage(),
				Error:            twitterDBErr.Error(),
			}
			dbErrs = append(dbErrs, &embErr)
			continue
		} else {
			c.Log.Log(fmt.Sprintf("SUCCESS: Twitter post added to DB for Discord Message ID: %s", request.Discord.MessageID), c.Log.LogInformation)
		}
	}

	var twitterIDs []string
	var twitterUrls []string

	for _, aTweet := range tweets {
		twitterIDs = append(twitterIDs, aTweet.IdStr)

		urls := services.GetTweetUrls(aTweet)

		for _, url := range urls {
			twitterUrls = append(twitterUrls, url)
		}
	}

	output := viewmodels.CreateTweetResponse{
		Status:      http.StatusCreated,
		Message:     "Successfully posted Discord images to Twitter",
		TwitterIDs:  twitterIDs,
		TwitterUrls: twitterUrls,
	}

	if len(twitterIDs) == 0 {
		output.Status = http.StatusBadRequest
		output.Message = "Failed to post Discord images to Twitter"
	}

	if len(dbErrs) > 0 {
		output.DBErrors = dbErrs
	}

	if len(tweetErrs) > 0 {
		output.TwitterErrors = tweetErrs
	}

	SendJSONResponse(w, output, output.Status)
}

// DeleteTweet creates a Twitter post
func (c *Controller) DeleteTweet(w http.ResponseWriter, r *http.Request) {
	c.Log.Log("PROCESS: DeleteTweet route called", c.Log.LogInformation)

	if r.Header.Get("Auth-Token") != c.AuthToken {
		c.Log.Log("BLOCKED: Invalid auth token for DeleteTweet request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid auth token", "Invalid auth token for DeleteTweet request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, err.Error(), "Could not read body of request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	var request viewmodels.DeleteTweetRequest
	json.Unmarshal(reqBody, &request)

	c.Log.Log("PROCESS: DeleteTweet request validation", c.Log.LogInformation)

	validationErr := request.Validate()

	if validationErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: DeleteTweet request validation failed: %s", validationErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, validationErr.Error(), validationErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	c.Log.Log("SUCCESS: DeleteTweet request validation passed", c.Log.LogInformation)

	twitterClient, clientErr := services.GetTwitterClient(request.Discord.GuildID)

	if clientErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to create Twitter client: %s", clientErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, clientErr.Error(), "Unable to create Twitter client")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	twitter := services.Twitter{
		Client: twitterClient,
	}

	dbStruct, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbStruct.GetDB().Close()

	var twitterErrs []*viewmodels.EmbeddedError
	var deleteErrs []*viewmodels.EmbeddedError
	var discordMessageIDs []string

	for _, messageID := range request.Discord.MessageIDs {

		twitterModel := models.Twitter{
			DiscordMessageID: messageID,
		}

		c.Log.Log(fmt.Sprintf("PROCESS: Getting all Twitter DB records for Discord Message ID: %s", messageID), c.Log.LogInformation)

		tweets, twitterDBGetErr := twitterModel.GetAll(dbStruct)

		if twitterDBGetErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Failed getting all Twitter DB records for Discord Message ID (%s) with error: %s", messageID, twitterDBGetErr.Error()), c.Log.LogInformation)

			embErr := viewmodels.EmbeddedError{
				DiscordMessageID: messageID,
				Message:          twitterDBGetErr.GetMessage(),
				Error:            twitterDBGetErr.Error(),
			}

			deleteErrs = append(deleteErrs, &embErr)
			continue
		}

		if len(tweets) == 0 {
			c.Log.Log("ERROR: No Twitter post found in database to delete", c.Log.LogInformation)
			continue
		}

		c.Log.Log(fmt.Sprintf("SUCCESS: Got all Twitter DB records for Discord Message ID: %s", messageID), c.Log.LogInformation)

		for _, aTweet := range tweets {
			twitterID, convErr := strconv.Atoi(aTweet.TwitterID)

			if convErr != nil {
				embErr := viewmodels.EmbeddedError{
					DiscordMessageID: messageID,
					Message:          fmt.Sprintf("Could not convert string to int for Twitter ID: %s", aTweet.TwitterID),
					Error:            convErr.Error(),
				}

				twitterErrs = append(twitterErrs, &embErr)
			}

			c.Log.Log(fmt.Sprintf("PROCESS: Deleting Twitter post with Twitter ID: %d", twitterID), c.Log.LogInformation)

			_, deleteErr := twitter.DeletePost(int64(twitterID))

			if deleteErr != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Failed to delete Twitter post with Twitter ID: %d", twitterID), c.Log.LogMedium)

				embErr := viewmodels.EmbeddedError{
					DiscordMessageID: messageID,
					Message:          fmt.Sprintf("Could not delete Twitter post with Twitter ID: %d", twitterID),
					Error:            deleteErr.Error(),
				}

				twitterErrs = append(twitterErrs, &embErr)
				continue
			}

			c.Log.Log(fmt.Sprintf("SUCCESS: Twitter post successfully deleted with Twitter ID: %d", twitterID), c.Log.LogInformation)

			c.Log.Log(fmt.Sprintf("PROCESS: Deleting Twitter post from DB with Discord Message ID: %s", messageID), c.Log.LogInformation)

			twitterDeleteModel := models.Twitter{
				ID: aTweet.ID,
			}

			deleteDBErr := twitterDeleteModel.Delete(dbStruct)

			if deleteDBErr != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Failed to delete Twitter post from DB with Discord Message ID: %s", messageID), c.Log.LogMedium)

				embErr := viewmodels.EmbeddedError{
					DiscordMessageID: messageID,
					Message:          deleteDBErr.GetMessage(),
					Error:            deleteDBErr.Error(),
				}
				deleteErrs = append(deleteErrs, &embErr)
			} else {
				c.Log.Log(fmt.Sprintf("SUCCESS: Deleted Twitter post from DB with Discord Message ID: %s", messageID), c.Log.LogInformation)
				discordMessageIDs = append(discordMessageIDs, messageID)
			}
		}
	}

	output := viewmodels.DeleteTweetResponse{
		Status:     http.StatusOK,
		Message:    "Successfully deleted Discord image from Twitter",
		DiscordIDs: discordMessageIDs,
	}

	if len(discordMessageIDs) == 0 {
		output.Status = http.StatusBadRequest
		output.Message = "Failed to delete Discord images from Twitter"
	}

	if len(deleteErrs) > 0 {
		output.DBErrors = deleteErrs
	}

	if len(twitterErrs) > 0 {
		output.TwitterErrors = twitterErrs
	}

	SendJSONResponse(w, output, output.Status)

}

package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/BIC_Dev/discord-twitter-service/viewmodels"
)

// GetStatus responds with the availability status of this service
func (c *Controller) GetStatus(w http.ResponseWriter, r *http.Request) {
	status := viewmodels.GetStatusResponse{
		Status:  200,
		Message: "Twitter service is available",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(status)
}

package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils"

	// Loading SQLite3 dialect
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// SQLite3 struct
type SQLite3 struct {
	DB *gorm.DB
}

// Connect starts initial connection to DB
func (sql3 *SQLite3) Connect(c *utils.Config) *utils.ModelError {
	dbPath := fmt.Sprintf("%s%s", c.SQLite3.Path, c.SQLite3.DBName)
	newDB, err := gorm.Open("sqlite3", dbPath)

	if err != nil {
		if err != nil {
			modelError := utils.NewModelError(err)
			modelError.SetMessage("Failed to connect to SQLite3 DB")
			modelError.SetStatus(500)

			return modelError
		}
	}

	sql3.DB = newDB

	return nil
}

// Migrate migrates a table
func (sql3 *SQLite3) Migrate(table interface{}) *utils.ModelError {
	sql3.DB.AutoMigrate(table)

	return nil
}

// GetDB gets the DB from the struct
func (sql3 *SQLite3) GetDB() *gorm.DB {
	return sql3.DB
}

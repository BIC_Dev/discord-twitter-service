package db

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils"
)

// Interface DB interface
type Interface interface {
	Connect(c *utils.Config) *utils.ModelError
	Migrate(table interface{}) *utils.ModelError
	GetDB() *gorm.DB
}

// GetDB gets the DB
func GetDB(config *utils.Config) (Interface, *utils.ModelError) {
	var DBStruct Interface

	switch config.DB.Type {
	case "SQLite3":
		SQLite3 := SQLite3{}
		err := SQLite3.Connect(config)

		if err != nil {
			modErr := utils.NewModelError(err)
			modErr.SetMessage("Unable to connect to SQLite3 DB")
			modErr.SetStatus(500)

			return nil, modErr
		}

		DBStruct = &SQLite3
	case "PostgreSQL":
		PostgreSQL := PostgreSQL{
			Username: os.Getenv("POSTGRES_USERNAME"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
		}

		err := PostgreSQL.Connect(config)

		if err != nil {
			modErr := utils.NewModelError(err)
			modErr.SetMessage("Unable to connect to PostgreSQL DB")
			modErr.SetStatus(500)

			return nil, modErr
		}

		DBStruct = &PostgreSQL
	default:
		modErr := utils.NewModelError(fmt.Errorf("Default case hit when instantiating DB"))
		modErr.SetMessage(fmt.Sprintf("Invalid DB type: %s", config.DB.Type))
		modErr.SetStatus(500)

		return nil, modErr
	}

	return DBStruct, nil
}

package services

import (
	"github.com/ChimeraCoder/anaconda"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils"
	"gitlab.com/BIC_Dev/discord-twitter-service/utils/db"
)

// Service information for the service
type Service struct {
	DB            db.Interface
	Config        *utils.Config
	Log           *utils.Log
	TwitterClient *anaconda.TwitterApi
}

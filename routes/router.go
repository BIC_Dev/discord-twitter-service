package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/discord-twitter-service/controllers"
)

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	router.HandleFunc("/status", c.GetStatus).Methods("GET")

	router.HandleFunc("/twitter", c.CreateTweet).Methods("POST")
	router.HandleFunc("/twitter", c.DeleteTweet).Methods("DELETE")
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router) {
	log.Fatal(http.ListenAndServe(":8080", router))
}
